function countLetter(letter, sentence) {

    /*
        Goal: Create a solution that would count the number of times a letter occurs in a string.
        Other things to consider: the input to the function must be a letter and a string. If the first input is not a letter, output to the user invalid input
    */

    let result = 0;
    
    for(let i = 0; i < sentence.length; i++){
        if(sentence.charAt(i) === letter){
             result += 1;
        }else if (letter.length > 1 && letter !== String){
            return undefined
        }
    }
    return result
   

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}

console.log(countLetter('e', 'Sophia Isabelle'));
console.log(countLetter(0, 'Sophia Isabelle'));



function isIsogram(text) {
    /*
        Given a string, create a solution here that will output true if given string is an isogram and false otherwise. Disregard character case.
        an isogram is a word with no repeating characters (ie. "city" is an isogram, "hello" is not)
    */

    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    // for(let i = 0; i < text.length; i++)
    //     if (text.substring(i + 1).includes(text.charAt(i))){
    //         return false;
    //     }else{
    //         return true;
    //     }

    const lowCasedString = text.toLowerCase()
    const result = lowCasedString.split('').every((x,y) => lowCasedString.indexOf(x)===y)

    return result
    
}

function purchase(age, price) {
    /*
        Instruction:

        Students aged 13 to 21 years old as well as senior citizens (65 and up) both enjoy a 20% discount on purchased goods. 
        Students below 13 years of age cannot purchase without their parent's consent. 
        Given an age and a price, create a solution that will output a warning if age is below permitted age or the final purchase price otherwise. Final purchase price must have two decimal places.
    */

    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    /*// let result = price
    let discountedPrice = Math.round(price * 0.2);
    let roundedOffPrice = Math.round(price);

    if ((age >= 13 && age <= 21) || (age >= 65)) {
        return discountedPrice.toString();
        // return Math.round(result*.8).then(toString(result))
    }
    else if (age >= 22 && age <= 64) {
        return roundedOffPrice.toString();
        // return Math.round(result).then(toString(result))
    }
    else if(age < 13){
        return undefined
    }*/

    if(age < 13){
        return undefined;
    }else if((age >= 13 && age <= 21) || (age >= 65)){
        discountPrice = (price - (price*.2)).toFixed(2)
        return String(discountPrice);
    }else{
        return String(price.toFixed(2))
    }

    
}

function findHotCategories(items) {

    /*
        Goal:
        Given an array of inventory items, create a solutions that will return an array of DISTINCT categories that have at least ONE of its items sold out.
        If all items are in stock, return a notification message instead. 

    */
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    const itemCategory = [];
    let stock = items.map(items => {
    if (items.stocks == 0) {
        let withStocks = itemCategory.indexOf(items.category);
        if (withStocks ==-1) {
        itemCategory.push(items.category)
        }
    }
   });
  return itemCategory
}


function findFlyingVoters(candidateA, candidateB) {

    /*
        Goal:

        Given 2 arrays of voter ID's representing the votes of two different candidates, create a solution that will return an array of voter ID's who voted for BOTH candidates. 
        If no flying voters identified, return an "All good!" notification message instead.
    */
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    let votersId = []

    for(let x = 0; candidateA.length > x; x++){
        for(let y = 0; candidateB.length > y; y++){
            if(candidateA[x] == candidateB[y]){
                votersId.push(candidateA[x]);
            }
        }
    }
    if(votersId.length > 0){
        return votersId;
    }else{
        return "All Good!"
    }

    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};